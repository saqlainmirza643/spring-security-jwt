package com.saqlain.springsecurityjwt.service.impl;

import com.saqlain.springsecurityjwt.config.JwtService;
import com.saqlain.springsecurityjwt.entity.AppUser;
import com.saqlain.springsecurityjwt.entity.Authority;
import com.saqlain.springsecurityjwt.model.AuthenticationRequest;
import com.saqlain.springsecurityjwt.model.AuthenticationResponse;
import com.saqlain.springsecurityjwt.model.RegisterRequest;
import com.saqlain.springsecurityjwt.repo.AppUserRepo;
import com.saqlain.springsecurityjwt.repo.AuthorityRepo;
import com.saqlain.springsecurityjwt.service.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AppUserServiceImpl implements AppUserService {
    private final AppUserRepo userRepo;
    private final AuthorityRepo authorityRepo;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;
    @Override
    public boolean checkUsernameExists(String username) {
        return userRepo.findByUsername(username).isPresent();
    }
    @Override
    public AppUser registerUser(RegisterRequest user) {
        AppUser newUser = new AppUser();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(passwordEncoder.encode(user.getPassword()));
        Set<Authority> authorities = new HashSet<>();
        for (String authorityName : user.getAuthorities()) {
            Authority authority = authorityRepo.findByName(authorityName);
            if (authority != null) {
                authorities.add(authority);
            }
        }
        newUser.setAuthorities(authorities);
        return userRepo.save(newUser);
    }
    @Override
    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        var user=userRepo.findByUsername(request.getUsername()).orElseThrow();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(),request.getPassword()));
        var jwt=jwtService.generateToken(user);
        return AuthenticationResponse.builder().token(jwt).build();
    }
}


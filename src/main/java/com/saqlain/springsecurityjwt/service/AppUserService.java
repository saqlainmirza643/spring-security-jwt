package com.saqlain.springsecurityjwt.service;

import com.saqlain.springsecurityjwt.entity.AppUser;
import com.saqlain.springsecurityjwt.model.AuthenticationRequest;
import com.saqlain.springsecurityjwt.model.AuthenticationResponse;
import com.saqlain.springsecurityjwt.model.RegisterRequest;

public interface AppUserService {
    boolean checkUsernameExists(String username);
    AppUser registerUser(RegisterRequest request);
    AuthenticationResponse authenticate(AuthenticationRequest request);
}

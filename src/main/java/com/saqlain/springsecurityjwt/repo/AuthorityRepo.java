package com.saqlain.springsecurityjwt.repo;

import com.saqlain.springsecurityjwt.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepo extends JpaRepository<Authority, Integer> {
    Authority findByName(String authorityName);
}

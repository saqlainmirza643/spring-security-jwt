package com.saqlain.springsecurityjwt.controller;

import com.saqlain.springsecurityjwt.entity.AppUser;
import com.saqlain.springsecurityjwt.model.AuthenticationRequest;
import com.saqlain.springsecurityjwt.model.AuthenticationResponse;
import com.saqlain.springsecurityjwt.model.RegisterRequest;
import com.saqlain.springsecurityjwt.service.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class AppUserController {
    private final AppUserService appUserService;
    @GetMapping("/test")
    public String getMessage() {
        return "hello";
    }
    @GetMapping("/user/test")
    public String getPublicMessage() {
        return "public message";
    }
    @GetMapping("/public/expired")
    public String getExpiredMessage() {
        return "page is expired";
    }
    @PostMapping("/public/register")
    public ResponseEntity<?> register(@RequestBody RegisterRequest request) {
        boolean isExist = appUserService.checkUsernameExists(request.getUsername());
        if (isExist) {
            return ResponseEntity.badRequest().body("Username already exists");
        }
        AppUser registeredUser = appUserService.registerUser(request);
        return ResponseEntity.ok().body(registeredUser);
    }
    @PostMapping("/public/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request){
        return ResponseEntity.ok(appUserService.authenticate(request));
    }
}
